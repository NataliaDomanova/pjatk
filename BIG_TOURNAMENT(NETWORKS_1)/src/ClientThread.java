import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.Scanner;

public class ClientThread extends Thread {

    Socket client = null;
    Participant p;



    public ClientThread(Participant p) {
        super();

        try {
            client = new Socket(p.ip, p.port);
        } catch (IOException e) {
            System.out.println("No I/O ma1");
        }

        this.p = p;


    }

    public void run() {
        BufferedReader reader = null;
        PrintWriter print = null;

        try {
            reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
            print = new PrintWriter(client.getOutputStream(), true);
        } catch (IOException e) {
            System.out.println("Cannot get stream na1");
        }

        print.println(Main.myName+" is playing with you db1");

        String text = null;


        String line = null;

        try {
            line = reader.readLine();
        } catch (IOException e) {
            System.out.println("Cannot listen oa1");
        }



        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int b = 0;
        try {
            b = QuitThread.askNumber("Please, choose a number");
        } catch (IOException e) {
            e.printStackTrace();
        }

        int en = 1 + (int)(Math.random() * (100));

        print.println(b*en);

        int a = 0;

        try {
            a = Integer.parseInt(reader.readLine());
        } catch (IOException e) {
            System.out.println("cannot listen pa1");
        }

        double rand = (Math.random());

        if (rand < 0.5) {

            if ((a+b) % 2 == 1) {
                System.out.println("You lost ra1");
                print.println("You won!");
                Main.resultBuilder.append(Main.myName+" 0 : "+p.name+" 1");
            } else {
                System.out.println("You won ra1");
                print.println("You lost!");
                Main.myScore+= 1;
                Main.resultBuilder.append(Main.myName+" 1 : "+p.name+" 0");
            }
        } else {
            if ((a+b) % 2 == 1) {
                System.out.println("You won sa1");
                print.println("You lost! eb1");
                Main.myScore+= 1;
                Main.resultBuilder.append(Main.myName+" 1 : "+p.name+" 0");
            } else {
                System.out.println("You lost sa1");
                print.println("You won! eb1");
                Main.resultBuilder.append(Main.myName+" 0 : "+p.name+" 1");
            }
        }



        p.hasPlayed = true;

        try {
            client.close();
            Main.queue.pop();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
