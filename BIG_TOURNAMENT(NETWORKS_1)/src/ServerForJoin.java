import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerForJoin extends Thread {

    Socket agent;

    public ServerForJoin(Socket client) {
        super();
        this.agent = client;
    }

    public void run() {

        try {

            BufferedReader in = new BufferedReader(new InputStreamReader(agent.getInputStream()));
            PrintWriter out = new PrintWriter(agent.getOutputStream(), true);

            String line = in.readLine();

            if (line.contains("JOIN")) {
                System.out.println(line);
                String[] lines = line.split(" ");
                out.print(Main.me.toString());
                if (Main.participants != null) {
                    out.println(Main.printListOfParticipants());
                }

                System.out.println(Main.me.toString());
                System.out.println(Main.printListOfParticipants());

                out.println();
                Participant p = new Participant(lines[1], agent.getInetAddress().getHostAddress(), Integer.parseInt(lines[2]),
                        Integer.parseInt(lines[3]),
                        false);
                if (!Main.participants.containsKey(p.toString())) {
                    Main.participants.put(p.toString(), p);
                }

            }

            if (line.contains("left")) {
                line = in.readLine();
                System.out.println("line " +line);
                System.out.println("list of part");
                System.out.println(Main.printListOfParticipants());
                System.out.println("new list");
                Main.participants.remove(line+"\r\n");
                System.out.println(Main.printListOfParticipants());
            }
        } catch (IOException e) {

            System.out.println("cannot listen ja1");

        }

        try {
            agent.close();
        } catch (IOException e) {
            System.out.println("CANNOT CLOSE AGENT");
        }
    }
}
