import java.net.InetAddress;

public class Participant {
    String name;
    String ip;
    int port;
    int IPort;
    boolean hasPlayed;
    int score = 0;

    Participant(String name, String ip, int port, int IPort, boolean hasPlayed) {
        this.name = name;
        this.ip = ip;
        this.port = port;
        this.IPort = IPort;
        this.hasPlayed = hasPlayed;
    }

    @Override
    public String toString() {
        return this.name+" "+this.ip+" "+this.port+" "+this.IPort+"\r\n";
    }
}
