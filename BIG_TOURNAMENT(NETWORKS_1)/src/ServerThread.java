import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerThread extends Thread {

    ServerSocket server;
    boolean type;


    public ServerThread(ServerSocket server, boolean b) {
        super();
        this.server = server;
        System.out.println("this port "+this.server.getLocalPort());
        type = b;

    }

    public  void run() {
        Socket client = null;

        while(true) {
            try {
                client = server.accept();
            }
            catch (IOException e) {
                System.out.println("Accept failed ka1");
                System.exit(-1);
            }
            if (type) {
                (new ServerForJoin(client)).start();

            } else  {
                GameThread thread = new GameThread(client);
                thread.start();
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("The loop is still working la1");
            }
        }

    }
}
