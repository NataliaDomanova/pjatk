import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.SynchronousQueue;

public class Main {

    static String myName;
    static String myIp;
    static int myIPort;
    static int myPort;
    static Map<String, Participant> participants;
    static Participant me;
    static LinkedList<Participant> queue = new LinkedList<>();
    static int myScore = 0;
    static StringBuilder resultBuilder = new StringBuilder();

    public static void main(String[] args) throws IOException, InterruptedException {

        new QuitThread().start();

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        // System.out.println("What is your name? aa1");
        myName = QuitThread.askString("What is your name?");
        //  chechForQuit(myName);

        ServerSocket serverJoin = null;
        ServerSocket server = null;
        Socket socket = null;
        PrintWriter out = null;
        BufferedReader in = null;



        try{
            serverJoin = new ServerSocket(0, 1, InetAddress.getLocalHost());
            server = new ServerSocket(0, 1, InetAddress.getLocalHost());
            myPort = server.getLocalPort();
            myIPort = serverJoin.getLocalPort();
            participants = new HashMap<String, Participant>();
            myIp = server.getInetAddress().getHostAddress();


        } catch (IOException e) {
            System.out.println("Could not listen");
            System.exit(-1);
        }
        System.out.println("Server listens on port: " + serverJoin.getLocalPort()+" "+
                "Server IP: "+server.getInetAddress().getLocalHost() + "ba1");

        new ServerThread(serverJoin, true).start();

        me = new Participant(myName, myIp , myPort, myIPort, true);
        System.out.println(me.toString());

        // System.out.println("would you like to join somebody? ca1");
        String i = QuitThread.askString("Would you like to join somebody?");
        //  chechForQuit(i);

        if (i.equals("yes")) {



            String address = QuitThread.askString("Enter the IP address to join");
            int port = QuitThread.askNumber("Enter the port number to join");
            try {
                socket = new Socket(address, port);


                out = new PrintWriter(socket.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            } catch (UnknownHostException e) {
                System.out.println("Unknown host fa1");
                System.exit(-1);
            } catch (IOException e) {
                System.out.println("No I/O ga1");
                System.exit(-1);
            }

            out.println("JOIN " + myName +" " + myPort +" "+myIPort);


            String line;
            while ((line = in.readLine()) != null && !line.isEmpty()) {
                String[] lines = line.split(" ");
                Participant p = new Participant(lines[0], socket.getInetAddress().getHostAddress(),
                        Integer.parseInt(lines[2]), Integer.parseInt(lines[3]), false);
                if (!participants.containsKey(p.toString())&&(!me.toString().equals(p.toString()))) {
                    Main.participants.put(p.toString(), p);
                    queue.push(p);
                }

            }

            try {
                socket.close();
            } catch (IOException e) {
                System.out.println("Cannot close the socket ha1");
                System.exit(-1);
            }
        }

        for (Map.Entry<String, Participant> map: participants.entrySet()) {
            ClientThread thread = new ClientThread(map.getValue());
            thread.start();
            try {
                thread.join();
            } catch (InterruptedException e) {
                System.out.println("Interrupted ia1");
            }
        }

        (new ServerThread(server, false)).start();




    }

    static String printListOfParticipants() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, Participant> p: participants.entrySet()) {
            sb.append(p.getKey());
        }
        return sb.toString();
    }


    static public void chechForQuit(String s) {
        if (s.equals("QUIT")) System.exit(0);
    }
}
