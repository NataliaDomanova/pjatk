import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;
import java.util.Scanner;

public class QuitThread  extends Thread {

    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    boolean isReading;

    public QuitThread() {
        super();
        isReading = true;
    }

    public void run() {

        while (true) {
            try {
                if (reader.ready()) {
                    try {
                        ifQuit(reader.readLine());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String askString(String s) throws IOException {
        // isReading = false;
        System.out.println(s);
        String answer = reader.readLine();
        while (!ifQuit(answer)) {
            System.out.println(s);
            answer = reader.readLine();
        }
        // isReading = true;
        return answer;

    }

    public static int askNumber(String s) throws IOException {
        // isReading = false;
        System.out.println(s);
        String answer = reader.readLine();
        while (!ifQuit(answer)) {
            System.out.println(s);
            answer = reader.readLine();
        }
        //  isReading = true;
        return Integer.parseInt(answer);
    }

    public static boolean  ifQuit(String s) {
        if (s.equals("QUIT")) {
            if (Main.queue.isEmpty()) {
                if (Main.participants!= null) {
                    System.out.println(Main.resultBuilder.toString());
                    for (Map.Entry<String, Participant> map : Main.participants.entrySet()) {
                        Participant p = map.getValue();
                        Socket socket = null;
                        PrintWriter pw = null;
                        try {
                            socket = new Socket(p.ip, p.IPort);
                            pw = new PrintWriter(socket.getOutputStream(), true);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        pw.println("left");
                        pw.println(Main.me.toString());
                    }
                }
                System.out.println("GOODBYE <3");
                System.exit(0);
            } else {
                System.out.println("Sorry, you can't quit");
                return false;
            }
        }
        return true;
    }
}
