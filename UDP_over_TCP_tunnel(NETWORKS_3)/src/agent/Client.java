package agent;

import java.net.DatagramSocket;

public class Client {
    String ip;
    int port;
    DatagramSocket socket;

    public Client(int port, String ip, DatagramSocket socket) {
        this.ip = ip;
        this.port = port;
        this.socket = socket;
    }
}
