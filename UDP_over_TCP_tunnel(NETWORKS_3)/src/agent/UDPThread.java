package agent;

import java.io.IOException;
import java.net.*;

public class UDPThread extends Thread {

    DatagramSocket socket;

    public UDPThread(int port) {
        super();

        try {
            socket = new DatagramSocket(port);
            Main.portsUDP.add(this);
        } catch (SocketException e) {
            System.out.println("UDP thread: can't open a socket");
        }
    }

    public void run() {
        while(!this.isInterrupted()) {
            receiveDatagram();
        }
    }

    private void receiveDatagram() {
        byte[] buff = new byte[508];
        final DatagramPacket datagram = new DatagramPacket(buff, buff.length);

        try {
            socket.receive(datagram);
            Client client = new Client( datagram.getPort(), datagram.getAddress().getHostAddress(), socket);
            Main.clients.put(socket.getLocalPort(), client);
            TCPClient.sendToRelay(datagram, socket.getLocalPort());
        } catch (IOException e) {
            System.out.println("UDP thread: can't receive datagram");
            System.exit(0);
        }







    }

    public static void sendDatagram(int port, String data) throws IOException {

        Client client = Main.clients.get(port);

        byte[] buff = String.valueOf(data).getBytes();
        final DatagramPacket datagram = new DatagramPacket(buff, buff.length, InetAddress.getByName(client.ip),
                client.port);



        client.socket.send(datagram);
        System.out.println("I sent a datagram: "+data);

        Main.clients.remove(port);



    }
}
