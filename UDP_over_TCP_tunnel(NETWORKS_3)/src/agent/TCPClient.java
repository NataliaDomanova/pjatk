package agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.Socket;

public class TCPClient {

    static Socket socket;
    static BufferedReader reader;
    static PrintWriter writer;

    public TCPClient(int port, String ip, String recipientIP) {
        socket = null;
        reader = null;
        writer = null;

        try {

            socket = new Socket(ip, port);
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new PrintWriter(socket.getOutputStream(), true);

        } catch (IOException e) {
            System.out.println("TCP client: can't open a socket");
        }

        writer.println("Hello!");
        String sline;
        try {
            sline = reader.readLine();
        } catch (IOException e) {
            System.out.println("TCP client: can't read a line");
        }

        writer.println(recipientIP);



            new Thread(() -> {
                try {

                        String line;
                               while ((line = reader.readLine()) != null) {
                                   System.out.println("TCP got a line " + line); //delete
                                   String[] lines = line.split(" ");
                                   UDPThread.sendDatagram(Integer.parseInt(lines[0]), lines[1]);
                               }
                } catch (IOException e) {
                    System.out.println("TCP client: can't read a line");
                }
            }).start();

    }

    public static void sendToRelay(DatagramPacket datagram, int UDPport) {
        writer.println(UDPport+" "+new String(datagram.getData(), 0, datagram.getLength()));
        System.out.println("TCP send a datagram "+new String(datagram.getData(), 0, datagram.getLength()));

    }

    public static void sendQuit() {
        writer.println("DISCONNECT");
    }

    public static void close() throws IOException {
        reader.close();
        writer.close();
        socket.close();

    }
}
