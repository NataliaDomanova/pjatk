package agent;

import relay.TCPThread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramSocket;
import java.net.Socket;
import java.util.Map;
import java.util.Scanner;

public class QuitThread  extends Thread {

    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public QuitThread() {
        super();
    }

    public void run() {

        while (true) {
            try {
                if (reader.ready()) {
                    try {
                        ifQuit(reader.readLine());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String askString(String question) throws IOException {
        System.out.println(question);
        String answer = reader.readLine();
        ifQuit(answer);
        return answer;

    }

    public static int askNumber(String question) throws IOException {
        System.out.println(question);
        String answer = reader.readLine();
        ifQuit(answer);
        return Integer.parseInt(answer);
    }

    public static void  ifQuit(String answer) throws IOException {
        if (answer.equals("QUIT")) {
            TCPClient.sendQuit();
            for (UDPThread socket: Main.portsUDP) {
                socket.interrupt();
                socket.socket.close();
            }
            TCPClient.close();
                System.out.println("GOODBYE <3");
                System.exit(0);
        }
    }
}
