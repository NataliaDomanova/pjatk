package agent;


import java.io.IOException;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {

    public static String relayIP;
    public static int relayPort;
    public static String recipientIP;
    public static ArrayList<UDPThread> portsUDP;
    public static HashMap<Integer, Client> clients;

    public static void main(String[] args) throws IOException {

        new QuitThread().start();
        clients = new HashMap<>();
        portsUDP = new ArrayList<>();

        relayIP = QuitThread.askString("Please, enter the IP address of a relay");
        relayPort = QuitThread.askNumber("Please, enter the port number of a relay");
        recipientIP = QuitThread.askString("Please, enter the IP address of a recipient");
        int numberOfPorts = QuitThread.askNumber("How many numbers of UDP ports needed to be open?");

        new TCPClient(relayPort, relayIP, recipientIP);

        for (int i = 0; i < numberOfPorts; i++) {
            int port = QuitThread.askNumber("Enter the "+(i+1)+" port number");
            new UDPThread(port).start();
        }
    }
}
