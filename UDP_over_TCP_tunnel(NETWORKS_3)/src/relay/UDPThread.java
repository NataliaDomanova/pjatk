package relay;

import java.io.IOException;
import java.net.*;

public class UDPThread extends Thread {

    static DatagramSocket socket;
    static InetAddress recipientIP;

    public UDPThread() {
        super();
        try {
            socket = new DatagramSocket();
        } catch (SocketException e) {
            System.out.println("UDP thread: can't open a socket");
        }
    }

    public void run() {
        while (true) {
            receive();
        }
    }

    private void receive() {
        byte[] buff = new byte[508];
        final DatagramPacket datagram = new DatagramPacket(buff, buff.length);

        try {
            System.out.println("I received a datagram from a recipient");
            socket.receive(datagram);
        } catch (IOException e) {
            System.out.println("UDP thread: can't receive a datagram");
        }

        if (datagram.getAddress().equals(recipientIP)) {
            new Thread(() -> {
                String data = new String(datagram.getData(), 0, datagram.getLength());
                System.out.println("That's what i received: "+data);
                TCPThread.sendData(datagram.getPort(), data);
            }).start();
        }
    }

    public static void sendDatagram(int port, String data){
        byte[] buff = String.valueOf(data).getBytes();
        DatagramPacket datagram = new DatagramPacket(buff, buff.length, recipientIP, port);

        try {
            socket.send(datagram);
            System.out.println("I sent a datagram to a recipient: "+data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
