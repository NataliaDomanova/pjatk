package relay;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class TCPServer {

    private ServerSocket server = null;
    private Socket client = null;

    public TCPServer(int port) throws UnknownHostException {
        try {
            server = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("Can't create TCP server");
        }

        System.out.println("Server listens on port: " + server.getLocalPort()+" "+
                "Server IP: "+server.getInetAddress().getLocalHost());

            while (true) {
                try {
                    client = server.accept();
                } catch (IOException e) {
                    System.out.println("Can't accept a client (TCP)");
                }

                new TCPThread(client).start();
            }
    }

}
