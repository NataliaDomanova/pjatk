package relay;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class TCPThread extends Thread {

    private Socket client;
    private String recipientIP;
    static BufferedReader reader;
    static PrintWriter writer;

    public TCPThread(Socket client) {
       super();
       this.client = client;
    }

    public void run() {

            try {
                reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
                writer = new PrintWriter(client.getOutputStream(), true);

                String line = reader.readLine();
                if (line.equals("Hello!")) {
                    writer.println("Hello back!");
                    line = reader.readLine();
                    recipientIP = line;
                    new UDPThread().start();
                    UDPThread.recipientIP = InetAddress.getByName(line);
                    System.out.println(UDPThread.recipientIP);

                    while (!(line = reader.readLine()).contains("DISCONNECT")) {
                        System.out.println("TCP got  " + line);
                        String[] lines = line.split(" ");
                        UDPThread.sendDatagram(Integer.parseInt(lines[0]), lines[1]);
                    }
                } else {
                    try {
                        reader.close();
                        writer.close();
                        client.close();
                    } catch (IOException e) {
                        System.out.println("TCP thread: can't close client");
                    }
                }


            } catch (IOException e) {
                e.printStackTrace();
            }

    }

    public static void sendData(int port, String data) {
        writer.println(port+" "+data);
    }
}
