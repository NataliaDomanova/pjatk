package relay;

import java.net.UnknownHostException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Please, enter the port number for TCP connection");
        Scanner scanner = new Scanner(System.in);
        int port = scanner.nextInt();

        try {
            new TCPServer(port);
        } catch (UnknownHostException e) {
            System.out.println("Main: can't open TCP");
        }
    }
}
