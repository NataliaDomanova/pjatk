package util;

import java.util.ArrayList;
import java.util.List;

public class Ports {

    private static List<Integer> ports = new ArrayList<>();


    public static void addPort(int s) {
        ports.add(s);
    }

    public static int getPort(int i) {
        return ports.get(i);
    }

    public static int getSize() {
        return ports.size();
    }
}
