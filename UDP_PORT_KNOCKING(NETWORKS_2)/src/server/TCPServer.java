package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class TCPServer {

    private ServerSocket server = null;
    private Socket client = null;

    public TCPServer() throws UnknownHostException {
        try {
            server = new ServerSocket(0, 1, InetAddress.getLocalHost());
        } catch (IOException e) {
            System.out.println("Can't create TCP server");
        }

        System.out.println("Server listens on port: " + server.getLocalPort()+" "+
                "Server IP: "+server.getInetAddress().getLocalHost());
    }

    public int getPort() {
        return server.getLocalPort();
    }

    public void listen() {
        try {
            client = server.accept();
        } catch (IOException e) {
            System.out.println("Can't accept a client (TCP)");
        }

        new TCPServerThread(client).start();
    }

}
