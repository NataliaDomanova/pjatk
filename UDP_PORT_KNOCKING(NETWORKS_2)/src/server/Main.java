package server;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static Map<String, Client> clients = new HashMap<>();

    public static void main(String[] args) throws UnknownHostException {

        Scanner scan = new Scanner(System.in);
        System.out.println("ENTER NUMBER OF PORTS");
        int n = scan.nextInt();

        System.out.println(InetAddress.getLocalHost());

        for (int i = 0; i < n; i++) {
            UDPServer server = new UDPServer();
            server.start();
        }

        scan.close();
    }
}
