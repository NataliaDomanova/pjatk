package server;

import util.Ports;


import java.net.InetAddress;

public class Client {
    private int port;
    private InetAddress IP;
    private int cursor = 0;
    private int[] knockPorts;

    public Client(int p, InetAddress ip) {
        port = p;
        IP = ip;
        knockPorts = new int[Ports.getSize()];
    }

    public void addPort(int port) {
        knockPorts[cursor] = port;
        cursor++;
    }

    boolean checkCursor() {
        return cursor >= knockPorts.length;
    }

    public boolean checkKnockedPorts() {
        for (int i = 0; i < knockPorts.length; i++) {
            if (knockPorts[i] != Ports.getPort(i)) return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.IP.toString()+" "+this.port;
    }

    public int getPort() { return port; }
    public InetAddress getIP() { return IP; }
}
