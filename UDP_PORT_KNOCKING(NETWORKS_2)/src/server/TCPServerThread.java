package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class TCPServerThread extends Thread {
    private Socket client;

    public TCPServerThread(Socket client) {
        super();
        this.client = client;
    }

    public void run() {

        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            PrintWriter out = new PrintWriter(client.getOutputStream(), true);

            String line = in.readLine();

            System.out.println(line);

            out.println("Yup! I'm here!");

            in.close();
            out.close();


        } catch (IOException e1) {
            System.out.println("Something went wrong... (TCP)");
        }

        try {
            client.close();
        } catch (IOException e) {
            System.out.println("Can't close client (TCP)");
        }
    }
}
