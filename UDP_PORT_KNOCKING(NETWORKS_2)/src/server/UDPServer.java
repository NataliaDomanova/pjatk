package server;

import util.Constants;
import util.Ports;


import java.io.IOException;
import java.net.*;

public class UDPServer extends Thread {

    private DatagramSocket server;

    public UDPServer() {
        try {
            server = new DatagramSocket();
            Ports.addPort(server.getLocalPort());
            System.out.println(" Port: " + server.getLocalPort());
        } catch (SocketException e) {
            System.out.println("Can't listen");
        }

    }

    public void run() {
        while (true) {
            try {
                execute();
            } catch (IOException e) {
                System.out.println("Something went wrong...");
            }
        }
    }

    public void execute() throws IOException {

        byte[] buff = new byte[Constants.MAX_DATAGRAM_SIZE];
        final DatagramPacket datagram = new DatagramPacket(buff, buff.length);

        server.receive(datagram);

        new Thread(() -> {
            System.out.println(new String(datagram.getData(), 0, datagram.getLength()));

            Client client = new Client(datagram.getPort(), datagram.getAddress());
            String key = client.toString();

            if (Main.clients.containsKey(key)) {
                client = Main.clients.get(key);
            } else {
                Main.clients.put(key, client);
            }

            client.addPort(server.getLocalPort());
            if (client.checkCursor()) {
                if (client.checkKnockedPorts()) {
                    try {
                        sendPort(client);
                        Main.clients.remove(key);
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();


    }

    public void sendPort(Client client) throws UnknownHostException {
        int clientPort = client.getPort();
        InetAddress clientAddress = client.getIP();

        TCPServer TCPserver = new TCPServer();
        int TCPport = TCPserver.getPort();

        byte[] buff = String.valueOf(TCPport).getBytes();
        DatagramPacket respond = new DatagramPacket(buff, buff.length, clientAddress, clientPort);

        try {
            server.send(respond);
        } catch (IOException e) {
            System.out.println("Can't send a respond");
        }

        TCPserver.listen();
    }
}
