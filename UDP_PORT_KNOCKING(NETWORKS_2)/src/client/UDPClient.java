package client;


import util.Constants;

import java.io.IOException;
import java.net.*;
import java.util.Scanner;

public class UDPClient {

    public UDPClient(String ip, int  numberOfPorts) throws IOException {

        InetAddress address = InetAddress.getByName(ip);
        Scanner scan = new Scanner(System.in);

        DatagramSocket socket = new DatagramSocket();

        for(int i = 0; i < numberOfPorts; i++) {
            System.out.println("ENTER "+(i+1)+ " PORT NUMBER\t");
            int line = scan.nextInt();

            byte[] queryBuff = String.valueOf("knock-knock").getBytes();
            DatagramPacket query = new DatagramPacket(queryBuff, queryBuff.length, address, line);
            socket.send(query);

        }

        socket.setSoTimeout(3000);
        byte[] buff = new byte[Constants.MAX_DATAGRAM_SIZE];
        DatagramPacket packet = new DatagramPacket(buff, buff.length);


            try {
                socket.receive(packet);
                String str = new String(packet.getData(), 0, packet.getLength()).trim();

                TCPClient client = new TCPClient(Integer.parseInt(str), address);



            } catch(SocketTimeoutException e) {
                System.out.println("TIMEOUT ERROR");
                System.exit(0);
            }


        socket.close();
        scan.close();

    }
}
