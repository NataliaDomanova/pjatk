package client;

import util.Ports;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("ENTER IP ADDRESS");
        Scanner scan = new Scanner(System.in);

        String ip = scan.nextLine();

        System.out.println("ENTER NUMBER OF PORTS");

        int numberOfPorts = scan.nextInt();


        //scan.close();

        try {
            new UDPClient(ip, numberOfPorts);
        } catch (IOException e) {
            System.out.println("UDP client error");
        }
    }
}
