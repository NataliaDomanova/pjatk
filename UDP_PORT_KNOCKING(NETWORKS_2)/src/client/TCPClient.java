package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class TCPClient {

    public TCPClient(int tcpPort, InetAddress address) throws IOException {
        Socket socket = null;
        int port = tcpPort;
        BufferedReader reader = null;
        PrintWriter writer = null;

        try {

            socket = new Socket(address, port);
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            System.out.println("Something went wrong... (TCPClient)");
        }

        writer.println("Hello, is anybody here?");

        String line = reader.readLine();

        System.out.println(line);

        writer.close();
        reader.close();

        socket.close();
    }
}
