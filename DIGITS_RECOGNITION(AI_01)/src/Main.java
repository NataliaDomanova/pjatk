import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;

public class Main {

    private static double[][] inputVectors;
    private static int[][] expectedValues;
    private static Random random;
    private static double[][] weightHidden1;
    private static double[][] weightHidden2;
    private static double[][] weightHidden3;
    private static double[][] weightHidden4;
    private static double[][] weightOutput;
    private static double[] biasHidden1;
    private static double[] biasHidden2;
    private static double[] biasHidden3;
    private static double[] biasHidden4;
    private static double[] biasOutput;
    private static double n = 0.5;
    private static double l = 0.5;

    public static void main(String[] args) {
        random = new Random(100);
        readDataSet("/Users/natalia/Documents/PJATK/4sem/NAI/projects/data/optdigits.tra.txt");
       double[][] outputVectors = new double[inputVectors.length][10];
        for (int e = 0; e < 1; e++) {
            for (int i = 0; i < inputVectors.length; i++) {
                outputVectors[i] = runNetwork(i);
                calculateErrors(i, outputVectors[i]);
            }
            System.out.println(e);
        }

        System.out.println("OUTPUT VECTORS:");

        int counter = 0;

        for (int i = 0; i < outputVectors.length; i++) {

            int foundClass = 0;
            int expectedClass = 0;
            for (int j = 0; j < outputVectors[i].length; j++) {
                if (outputVectors[i][j] == 1) {
                    foundClass = j;
                }
                if (expectedValues[i][j] == 1) {
                    expectedClass = j;
                }
            }
            if (expectedClass == foundClass) {
                System.out.print("EXPECTED CLASS: " + expectedClass);
                System.out.println("  CLASS: " + foundClass);
                counter++;
            }

        }

        System.out.println("RIGHT CLASSIFIED: "+counter+"/"+inputVectors.length);


    }

    private static double[] runNetwork(int i) {
        int rows1 = 32;
        int columns1 = 64;
        weightHidden1 = generateWeightMatrix(rows1, columns1);
        biasHidden1 = generateBiasMatrix(rows1);
        double[] net1 = calculateNet(weightHidden1, inputVectors[i], biasHidden1);
        double[] output1 = calculateOutputSigmoid(net1);

        int rows2 = 16;
        int columns2 = 32;
        weightHidden2 = generateWeightMatrix(rows2, columns2);
        biasHidden2 = generateBiasMatrix(rows2);
        double[] net2 = calculateNet(weightHidden2, output1, biasHidden2);
        double[] output2 = calculateOutputSigmoid(net2);

//        int rows3 = 18;
//        int columns3 = 25;
//        weightHidden3 = generateWeightMatrix(rows3, columns3);
//        biasHidden3 = generateBiasMatrix(rows3);
//        double[] net3 = calculateNet(weightHidden3, output2, biasHidden3);
//        double[] output3 = calculateOutputSigmoid(net3);
//
//        int rows4 = 14;
//        int columns4 = 18;
//        weightHidden4 = generateWeightMatrix(rows4, columns4);
//        biasHidden4 = generateBiasMatrix(rows4);
//        double[] net4 = calculateNet(weightHidden4, output3, biasHidden4);
//        double[] output4 = calculateOutputSigmoid(net4);

        int rows5 = 10;
        int columns5 = 16;
        weightOutput = generateWeightMatrix(rows5, columns5);
        biasOutput = generateBiasMatrix(rows5);
        double[] net5 = calculateNet(weightOutput, output2, biasOutput);
        return calculateOutputLinear(net5);
    }

    private static double[] calculateOutputSigmoid(double[] net) {
        double[] output = new double[net.length];
        for (int i = 0; i < output.length; i++) {
            output[i] = 2/(1+Math.exp((-l)*net[i]))-1;
        }
        return output;
    }

    private static double[] calculateOutputLinear(double[] net) {
        //using unipolar function
        double[] output = new double[net.length];
        double max = net[0];
        for (int i = 1; i < net.length; i++) {
            if (net[i] > max) {
                max = net[i];
            }
        }
        for (int i = 0; i< output.length; i++) {
            if (net[i] == max) {
                output[i] = 1;
            } else {
                output[i] = 0;
            }
        }
        return output;
    }

    private static void calculateErrors(int i, double[] outputVector) {
        double[] errorOutput = calculateErrorOutput(i, outputVector);
//        double[] errorHidden4 = calculateErrorHidden(i, errorOutput, weightOutput, weightHidden4, biasHidden4);
//        double[] errorHidden3 = calculateErrorHidden(i, errorHidden4, weightHidden4, weightHidden3, biasHidden3);
        double[] errorHidden2 = calculateErrorHidden(i, errorOutput, weightOutput, weightHidden2, biasHidden2);
        double[] errorHidden1 = calculateErrorHidden(i, errorHidden2, weightHidden2, weightHidden1, biasHidden1);

    }

    private static double[] calculateErrorOutput(int i, double[] outputVector) {
        double[] error = new double[10];
        for (int j = 0; j < 10; j++) {
            error[j] = (expectedValues[i][j] - outputVector[j]);
            correctPerceptron(error[j], weightOutput, outputVector, j);
            correctBias(error[j], biasOutput);
        }
        return error;
    }

    private static void correctBias(double error, double[] bias) {
        for (int i = 0; i < bias.length; i++) {
            bias[i] += n*error;
        }
    }

    private static void correctPerceptron(double error, double[][] weightMatrix, double[] outputVector, int j) {
        for (int i = 0; i < weightMatrix.length; i++) {
            weightMatrix[i][j] += n*error*outputVector[i];
        }
    }

    private static double[] calculateErrorHidden(int i, double[] errorOutput, double[][] outputMatrix, double[][] matrix, double[] bias) {
        double[] error = new double[errorOutput.length];
        for (int j = 0; j < error.length; j++) {
            for (int k = 0; k < outputMatrix[j].length; k++) {
                error[j] += errorOutput[j]*outputMatrix[j][k];
            }
            error[j] *= l/2*(1-inputVectors[i][j]*inputVectors[i][j]);
            correctPerceptron(error[j], matrix, inputVectors[i], j);
            correctBias(error[j], bias);
        }
        return error;
    }

    private static double[] calculateNet(double[][] weights, double[] vector, double[] bias) {
        double[] net = new double[weights.length];
        for (int i = 0; i < weights.length; i++) {
            for (int j = 0; j < weights[i].length; j++) {
                net[i] += weights[i][j] * vector[j];
            }
            net[i] += bias[i];
        }

        return net;
    }

    private static double[] generateBiasMatrix(int rows) {
        double[] bias = new double[rows];
        for (int i = 0; i < rows; i++) {
            bias[i] = random.nextDouble()*2-1;
        }
        return bias;
    }

    private static double[][] generateWeightMatrix(int rows, int columns) {
        double[][] matrix = new double[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                matrix[i][j] = random.nextDouble()*2-1;
            }
        }
        return matrix;
    }

    private static String readFile(String filePath) {
        File file = new File(filePath);
        if (!file.exists()) {
            try {
                throw new NoSuchFieldException();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
        byte[] encoded = new byte[0];
        try {
            encoded = Files.readAllBytes(Paths.get(file.getPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String(encoded, StandardCharsets.UTF_8);
    }

    private static void readDataSet(String filePath) {
        String data = readFile(filePath);
        readData(data);
    }

    private static void readData(String data) {
        String[] vectors = data.split("\n");
        getInputVectors(vectors);
        getExpectedValues(vectors);
    }

    private static void getExpectedValues(String[] vectors) {
        expectedValues = new int[vectors.length][10];
        for (int i = 0; i < vectors.length; i++) {
            String[] vector = vectors[i].split(",");
            switch (Integer.parseInt(vector[vector.length-1])) {
                case 0:
                    expectedValues[i][0] = 1;
                    break;
                case 1:
                    expectedValues[i][1] = 1;
                    break;
                case 2:
                    expectedValues[i][2] = 1;
                    break;
                case 3:
                    expectedValues[i][3] = 1;
                    break;
                case 4:
                    expectedValues[i][4] = 1;
                    break;
                case 5:
                    expectedValues[i][5] = 1;
                    break;
                case 6:
                    expectedValues[i][6] = 1;
                    break;
                case 7:
                    expectedValues[i][7] = 1;
                    break;
                case 8:
                    expectedValues[i][8] = 1;
                     break;
                case 9:
                    expectedValues[i][9] = 1;
                    break;
            }
        }
//        System.out.println("EXPECTED VALUES:");
//        for (int i = 0; i < expectedValues.length; i++) {
//            System.out.print("[ ");
//            for (int j = 0; j < expectedValues[i].length; j++) {
//                System.out.print(expectedValues[i][j]+" ");
//            }
//            System.out.println("]");
//        }
    }

    private static void getInputVectors(String[] vectors) {

        inputVectors = new double[vectors.length][vectors[0].split(",").length];
        System.out.println("DATA SET ROWS: "+inputVectors.length);
        System.out.println("DATA SET COLUMNS: "+inputVectors[0].length);
        for (int i = 0; i < inputVectors.length; i++) {
            String[] vector = vectors[i].split(",");
            for (int j = 0; j < inputVectors[i].length; j++) {
                inputVectors[i][j] = Double.parseDouble(vector[j]);
            }
        }
    }
}
