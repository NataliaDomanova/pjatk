import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;


public class Board extends JFrame {

    JPanel panel = new JPanel();
    Card select;
    ArrayList<String> rateList = new ArrayList<>();

    Board() {
        initBoard();
    }

    void initBoard() {
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        setPreferredSize(new Dimension(750, 590));



        JButton b1 = new JButton("New Game");
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panel.removeAll();
                initNewGame();
                panel.repaint();
                panel.validate();

            }
        });
        b1.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(b1);

        JButton b2 = new JButton("High Scores");
        b2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panel.removeAll();
                initRateList();
                panel.repaint();
                panel.validate();

            }
        });
        b2.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(b2);

        JButton b3 = new JButton("Exit");
        b3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        b3.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(b3);

        add(panel, BorderLayout.CENTER);

        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

    private void initRateList() {

        JLabel label = new JLabel("RATE");
        panel.add(label);

        for (String pair: rateList) {
            panel.add(new JLabel(pair));
        }

        JButton buttonBack = new JButton("Back");
        buttonBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panel.removeAll();
                initBoard();
                panel.repaint();
                panel.validate();
            }
        });
        panel.add(buttonBack);

    }

    private void initNewGame() {

        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        JLabel label = new JLabel("Select grid size");
        panel.add(label);

        String[] optsS = {"2", "4", "6"};
        int[] opts = {2, 4, 6}; // ArrayList?? get value(key==optsS[i])
        JComboBox comboBox = new JComboBox(optsS);
        panel.add(comboBox);

            JButton bSubmit = new JButton("OK");
        bSubmit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int i = comboBox.getSelectedIndex();
                panel.removeAll();
                initGame(opts[i]*2);
                panel.repaint();
                panel.validate();
            }
        });
        panel.add(bSubmit);
    }

    private void initGame(int i) {
        setLayout(new BorderLayout());
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JLabel label = new JLabel(String.format("%02d:%02d", 0, 0));
        long lastTickTime = System.currentTimeMillis();
        Timer timer;
        panel.add(label);
        timer = new Timer(100, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                long runningTime = System.currentTimeMillis() - lastTickTime;
                Duration duration = Duration.ofMillis(runningTime);
                long hours = duration.toHours();
                duration = duration.minusHours(hours);
                long minutes = duration.toMinutes();
                duration = duration.minusMinutes(minutes);
                long millis = duration.toMillis();
                long seconds = millis / 1000;
                millis -= (seconds * 1000);
                label.setText(String.format("%02d:%02d", minutes, seconds));
            }
        });

        timer.start();
        ArrayList<Card> cardList = new ArrayList<>();
        for (int j = 0; j < i/2; j++) {
            cardList.add(new Card(j));
            cardList.add(new Card(j));
        }
        Collections.shuffle(cardList);

        for(Card button: cardList) {
            button.setText(button.getData()+"");
        }

        for( Card button: cardList) {
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    button.setText("meow"+button.getData());

                    if (select == null) {
                        select = button;
                    }
                    else {
                       if (button.getData() == select.getData()) {
                           button.isMatch = true;
                           select.isMatch = true;
                           int delay = 500;
                           Timer timer= new Timer(delay, new ActionListener() {
                               @Override
                               public void actionPerformed(ActionEvent e) {
                                   button.setEnabled(false);
                                   select.setEnabled(false);
                                   select = null;
                               }
                           });
                           timer.setRepeats( false );
                           timer.start();

                       }
                       else {

                           int delay = 500;
                           Timer timer = new Timer( delay, new ActionListener(){
                               @Override
                               public void actionPerformed( ActionEvent e ){
                                   button.setText(button.getData()+"");
                                   select.setText(select.getData()+"");
                                   select = null;
                               }
                           } );
                           timer.setRepeats( false );
                           timer.start();


                       }

                    }
                    int counter = 0;
                    for (Card button: cardList) {
                        if (!button.isMatch)
                            counter++;
                    }
                    if (counter == 0) {
                        int delay = 500;
                        Timer timer = new Timer( delay, new ActionListener(){
                            @Override
                            public void actionPerformed( ActionEvent e ){
                                panel.removeAll();
                                initSaveRate(i, label.getText());
                                panel.repaint();
                                panel.validate();
                            }
                        } );
                        timer.setRepeats( false );
                        timer.start();
                    }
                }

            });
            panel.add(button);
        }

    }

    private void initSaveRate(int i, String time) {
        JLabel label = new JLabel("Enter your name");
        panel.add(label);

        JTextArea textArea = new JTextArea();
        panel.add(textArea);

        JButton save = new JButton("Save");
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!textArea.getText().equals("")) {
                    rateList.add(textArea.getText() + " " + i / 2 + " " + time);
                    panel.removeAll();
                    initBoard();
                    panel.repaint();
                    panel.validate();
                }
                else {
                    JOptionPane.showMessageDialog(panel, "Please, enter your name");
                }
            }
        });
        panel.add(save);
    }
}
