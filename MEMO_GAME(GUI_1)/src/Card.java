import javax.swing.*;

public class Card extends JButton {
    int data;
    boolean isMatch = false;

    Card(int data) {
        this.data = data;
    }

    int getData() {
        return this.data;
    }
}
