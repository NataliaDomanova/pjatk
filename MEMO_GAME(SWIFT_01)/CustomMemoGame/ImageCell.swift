//
//  ImageCell.swift
//  CustomMemoGame
//
//  Created by Natalia Domanova on 16/12/2019.
//  Copyright © 2019 PJATK. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var label: UILabel!
}
