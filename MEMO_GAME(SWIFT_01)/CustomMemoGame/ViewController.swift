//
//  ViewController.swift
//  CustomMemoGame
//
//  Created by Natalia Domanova on 06/12/2019.
//  Copyright © 2019 PJATK. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var imageNames = ["monica", "joey", "chandler", "gunther", "ross", "rachel", "phoebe", "mike"]
    var imagesToShow = ["monica", "joey", "chandler", "gunther", "ross", "rachel", "phoebe", "mike"]
    var broadcastCell: ImageCell?
    var broadcastImage = UIImage(named: "broadcast")
    var score = 0
    
    @IBOutlet weak var imageView: UICollectionView!
    @IBOutlet weak var labelScore: UILabel!
    @IBOutlet weak var sliderLevel: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageNames.shuffle()
        imagesToShow.shuffle()
        
        imageView.delegate = self
        imageView.dataSource = self
    }
    
    func setUpBroadcastCell(_ cell: ImageCell) {
        broadcastCell = cell
        broadcastCell!.image.image = broadcastImage
        broadcastCell!.label.text = "broadcast"
    }
    
    func setUpCell(_ cell: ImageCell, _ index: Int) {
        cell.image.image = UIImage(named: imageNames[index])
        cell.label.text = imageNames[index]
        cell.image.isHidden = true
        cell.backgroundColor = UIColor.black
        print(imageNames[index])
    }
    
    func isUnmatchedImage(_ image: ImageCell) -> Bool {
        return imagesToShow.contains(image.label.text!)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedCell = imageView.cellForItem(at: indexPath) as! ImageCell
        if selectedCell != broadcastCell {
            selectedCell.image.isHidden = false;
            if isMatch(selectedCell) {
                changeScore(10)
                imagesToShow.popLast()
                if imagesToShow.isEmpty {
                    showEndGameAlert()
                    refreshGame()
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.broadcastCell?.image.image = self.broadcastImage
                        selectedCell.image.isHidden = true;
                }
            } else {
                changeScore(-1)
                hideImageWithDelay(selectedCell.image, getLevelDelay())
            }
        }
    }
    
    func refreshGame() {
        changeScore(score * (-1))
        imageNames.shuffle()
        imagesToShow = ["monica", "joey", "chandler", "gunther", "ross", "rachel", "phoebe", "mike"]
        imagesToShow.shuffle()
    }
    
    func showEndGameAlert() {
        let alert = UIAlertController(title: "Congratulations!", message: "You score is \(score)", preferredStyle: .alert)
        let actionPlayAgain = UIAlertAction(title: "Play Again!", style: .default, handler: nil)
        
        alert.addAction(actionPlayAgain)
        present(alert, animated: true, completion: nil)
    }
    
    func getLevelDelay() -> Double {
        return Double(sliderLevel.maximumValue + sliderLevel.minimumValue - sliderLevel.value)
    }
    
    func changeScore(_ change: Int) {
        score = score + change
        labelScore.text = "Score: \(score)"
    }
    
    func hideImageWithDelay(_ image: UIImageView, _ delay: Double) {
        setUserIteractionOnCells(enabled: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            image.isHidden = true
            self.setUserIteractionOnCells(enabled: true)
        }
    }
    
    func isMatch(_ image: ImageCell) -> Bool {
        return image.label.text == imagesToShow.last
    }

    func setUserIteractionOnCells(enabled: Bool) {
        for i in 0...imageNames.count {
            let indexPath = IndexPath(row: i, section: 0)
            let cell = imageView.cellForItem(at: indexPath)
            cell?.isUserInteractionEnabled = enabled
        }
    }
    
    @IBAction
    func showBroadcastImage(_ buttonShowImage: UIButton) {
        if (!imagesToShow.isEmpty) {
            let strImageToShow = imagesToShow.last
            broadcastCell!.image.image = UIImage(named: strImageToShow!)
            broadcastCell!.label.text = strImageToShow
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageNames.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = imageView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! ImageCell
        
        if indexPath.item == imageNames.count / 2 {
            setUpBroadcastCell(cell)
        } else {
            if indexPath.item < imageNames.count / 2 {
                setUpCell(cell, indexPath.item)
            } else {
                setUpCell(cell, indexPath.item - 1)
            }
        }
        return cell
    }
    
}
